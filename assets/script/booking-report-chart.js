var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"],
        datasets: [{
            label: '# of Votes',
            data: [0, 12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)'
            ],
            borderColor: [
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)',
                'rgba(108, 92, 231,0.5)'
            ],
            borderWidth: 1
        },
        {
            label: '# of Votes',
            data: [0, 3, 12, 8, 10, 5, 8],
            backgroundColor: [
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)'
            ],
            borderColor: [
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)',
                'rgba(214, 48, 49,0.5)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});