/**
 * class DigitalHub
 */
function DigitalHub(){
	this.base_url = "http://localhost/digitalhub-html/views/"
}

/**
 * function to load an html file into a given div
 * @param  {[type]}
 */
DigitalHub.prototype.loadView = function(view){
	$("#" + view.div_id).load(this.base_url + view.url + ".html"); 
};

/**
 * function to add a view concept
 * On click function of the given object Id, the given page is rendered in the view div
 * @param  {[type]}
 * @return {[type]}
 */
// DigitalHub.prototype.route = function(route){
// 	$("$" + route.id).click(function(event) {
// 		$("#" + route.append_div_id).load(this.base_url + route.page + ".html"); 
// 	});
// };

var digitalHub = new DigitalHub();

digitalHub.loadView({
	div_id : "primary-navigation",
	url :  "top-nav"
});

digitalHub.loadView({
    div_id : "side-navigation",
    url :  "side-nav"
});

digitalHub.loadView({
    div_id : "slider-element",
    url :  "slider"
});


$(document).on('click', '.nav-element', function () {
	$('.nav-element').removeClass('active');
	$(this).addClass('active');
	console.log($(this).children().children()[0].src);
});



$(function() {
	$("#courier-booking").click(function(){
		$("#courier-booking-modal").css("display",'block');
	});

	$("#close-modal").click(function(){
		$("#courier-booking-modal").css("display",'none');
	});    

	$('#burger-menu').click(function(event) {
		 
          $('#side-nav').toggleClass('hide-nav');
          $('#top-nav').toggleClass('expand-nav');
          $('#sub-nav').toggleClass('expand-nav');
          $('#content-box').toggleClass('expand-nav');
	});
});